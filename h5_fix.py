import h5py as h5

paths = ["data/calorimetry_constant/calorimetry_constant.h5", "data/PDC004/PDC004.h5", "data/PDC012/PDC012.h5", "data/PDC030/PDC030.h5"]
for path in paths:
    f = h5.File(paths[0], "a")
    f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f"].attrs["name"] = "temperature_calorimeter_1"
    f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde"].attrs["name"] = "temperature_environment"
    f.close()